import xml.etree.ElementTree as ET

def parseList(xmlFileData):
    elemList = []
    listElem = []
    textElem = []
    lineCount = 1
    lineCnt = []
    for elem in xmlFileData.iter():
        elemList.append(elem.getchildren())
        listElem.append(elem.tag)
        textElem.append(elem.text)
        lineCnt.append(lineCount)
        lineCount = lineCount + 1
    return zip(elemList,listElem,textElem,lineCnt)

def makeStr(elementList):
    parseChild = []
    parseParent = []
    parseText = []
    parseLineNo = []
    for elem in elementList:
        parseChild.append(map(lambda x : str(x).split()[1],elem[0]))
        parseParent.append(elem[1])
        parseText.append(elem[2])
        parseLineNo.append(elem[3])
    return zip(parseParent,parseChild,parseText,parseLineNo)

def findUniqueId(listElem):
    idList = []
    idName = []
    idList = filter(lambda x : x[0]=='objid',listElem)
    for idElem in idList:
        idName.append(idElem[2])
   # print idName
    return idList

xmlFileData = ET.parse('rooms.xml')
elemList = parseList(xmlFileData)
listElem = makeStr(elemList)


